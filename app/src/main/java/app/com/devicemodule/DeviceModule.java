package app.com.devicemodule;

import android.app.Activity;

/**
 * <p>Класс - модуль для работы с устройством</p>
 * <p>С его помощью будут отловлены события нажатия на клавишу назад,
 * открытие и исчезновение клавиатура и тд.</p>
 * <p>Примечание - методы должны использовать друг друга по возможности для уменьшения кол-ва кода</p>
 */
public class DeviceModule{

    /**
     * <p>Уникальное имя модуля</p>
     */
    public static final String NAME = "DEVICE_MODULE";

    /**
     * <p>События генерируемые модулем</p>
     */
    public static final String BACK_PRESSED = "back_pressed";
    public static final String KEYBOARD_ONSHOW = "keyboard_onshow";
    public static final String KEYBOARD_ONHIDE = "keyboard_onhide";

    private Activity activity;

    public DeviceModule(Activity _activity){
        activity = _activity;
    }

    /**
     * <p>Генерирует евент клика на кнопку назад</p>
     */
    public void backPressed(){
        //once(NAME, BACK_PRESSED, "");
    }

    /**
     * <p>Генерирует евенты открытия - закрытия клавиатуры</p>
     */
    public void setKeyboardListener(){
        //TO DO
        //once(NAME, KEYBOARD_ONSHOW, "");
        //once(NAME, KEYBOARD_ONHIDE, "");
    }

}
