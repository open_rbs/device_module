package app.com.devicemodule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private DeviceModule deviceModule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        deviceModule = new DeviceModule(this);

        test();
    }

    public void test(){
        if(DeviceTests.test(deviceModule)){
            Log.d("MAIN_ACTIVITY", "WELL DONE");
        }else{
            Log.d("MAIN_ACTIVITY", "SOME ERRORS");
        }
    }
}
